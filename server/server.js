require("../config/config");
const express = require("express");
const path = require("path");

const app = express();
app.set("view engine","ejs");
app.set("views","../public/views");

app.use(express.static(path.join(__dirname, "../public")));

/* Setup server */
app.listen(process.env.PORT, () => {
  console.log(`Server is up on port ${process.env.PORT}!`);
});

app.get("/",function(req,res){
  res.render("home");
})